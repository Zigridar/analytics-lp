import React from 'react';
import { HeroBlock } from '../hero-block/HeroBlock.tsx';
import styles from './main.module.scss';
import { Features } from '../features/Features.tsx';
import { Pricing } from '../pricing/Pricing.tsx';

export const Main: React.FC = () => (
  <main className={styles.main}>
    <HeroBlock />
    <Features />
    <Pricing />
  </main>
);
