import React from 'react';
import styles from './pricing.module.scss';
import { Cards } from './components/Cards.tsx';
import { animate } from '../../utils/animate.ts';

export const Pricing: React.FC = () => (
  <section className={styles.section}>
    <div {...animate({ animation: 'zoom-in' })} className={styles.textWrapper}>
      <h2 className={styles.title}>Pricing Plans</h2>
      <p className={styles.sub}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum nisi
        aliquet volutpat pellentesque volutpat est.
      </p>
    </div>
    <Cards />
  </section>
);
