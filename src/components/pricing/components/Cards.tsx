import React, { useEffect, useRef } from 'react';
import styles from './cards.module.scss';
import { Button } from '../../button/Button.tsx';
import { animate } from '../../../utils/animate.ts';

type Card = {
  title: string;
  userCount: number;
  price: number;
};

const cards: Array<Card> = [
  {
    title: 'Starter',
    userCount: 3,
    price: 29,
  },
  {
    title: 'Standard',
    userCount: 20,
    price: 99,
  },
  {
    title: 'Premium',
    userCount: 200,
    price: 299,
  },
];

export const Cards: React.FC = () => {
  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (containerRef.current) {
      const container = containerRef.current;
      let mouseDown = false;
      let startX = 0;
      let scrollLeft = 0;

      const handleDown = ({ pageX }: MouseEvent) => {
        mouseDown = true;
        startX = pageX - container.offsetLeft;
        scrollLeft = container.scrollLeft;
      };

      const handleUp = () => {
        mouseDown = false;
      };

      const handleMove = ({ pageX }: MouseEvent) => {
        if (!mouseDown) return;
        const x = pageX - container.offsetLeft;
        const scroll = x - startX;
        container.scrollLeft = scrollLeft - scroll;
      };

      container.addEventListener('mousedown', handleDown);
      container.addEventListener('mouseup', handleUp);
      container.addEventListener('mousemove', handleMove);
      return () => {
        container.removeEventListener('mousedown', handleDown);
        container.removeEventListener('mouseup', handleUp);
        container.removeEventListener('mousemove', handleMove);
      };
    }
  }, []);

  return (
    <div ref={containerRef} className={styles.cardContainer}>
      <div className={styles.track}>
        {cards.map(({ title, userCount, price }, index) => (
          <div
            {...animate({
              animation: 'flip-down',
              duration: 800,
              offset: 200,
              delay: (1 + index) * 150,
            })}
            key={title}
            className={styles.card}
          >
            <h3 className={styles.title}>{title}</h3>
            <p className={styles.userCount}>up to {userCount} users</p>
            <p className={styles.price}>
              $<span className={styles.amount}>{price}</span>
            </p>
            <p className={styles.perMonth}>per mouth</p>
            <Button className={styles.button}>Order</Button>
          </div>
        ))}
      </div>
    </div>
  );
};
