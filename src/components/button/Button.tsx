import React, { HTMLAttributes, PropsWithChildren } from 'react';
import cn from 'classnames';
import styles from './button.module.scss';

type ButtonType = 'ghost' | 'red';

type ButtonProps = IClassName & {
  type?: ButtonType;
} & HTMLAttributes<HTMLButtonElement>;

export const Button: React.FC<PropsWithChildren<ButtonProps>> = ({
  className,
  children,
  type = 'red',
  ...rest
}) => (
  <button
    className={cn(className, styles.button, {
      [styles.buttonGhost]: type === 'ghost',
    })}
    {...rest}
  >
    {children}
  </button>
);
