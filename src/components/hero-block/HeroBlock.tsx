import React from 'react';
import styles from './hero-block.module.scss';
import { Button } from '../button/Button.tsx';
import mainChart from '../../assets/images/main-chart.png';
import mainScreen from '../../assets/images/main-screen.png';
import subtract from '../../assets/images/subtract.svg';
import { animate, withStartDelay } from '../../utils/animate.ts';

export const HeroBlock: React.FC = () => (
  <section className={styles.section}>
    <div className={styles.sectionContainer}>
      <div className={styles.placeholderContainer}>
        <img className={styles.topLeft} src={subtract} alt='svg' />
        <img className={styles.topRight} src={subtract} alt='svg' />
      </div>
      <div className={styles.chartContainer}>
        <img
          {...animate({ animation: 'fade-left', delay: withStartDelay(200) })}
          className={styles.mainScreen}
          src={mainScreen}
          alt='main screen'
        />
        <img
          {...animate({ animation: 'fade-left', delay: withStartDelay(350) })}
          className={styles.mainChart}
          src={mainChart}
          alt='chart'
        />
      </div>
      <div
        {...animate({ animation: 'fade-right', delay: withStartDelay() })}
        className={styles.textContainer}
      >
        <h1 className={styles.header}>
          Monitor your business on real-time dashboard
        </h1>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum
          nisi aliquet volutpat pellentesque volutpat est. Sapien in etiam vitae
          nibh nunc mattis imperdiet sed nullam.
        </p>
        <Button className={styles.button}>Try for free</Button>
      </div>
    </div>
  </section>
);
