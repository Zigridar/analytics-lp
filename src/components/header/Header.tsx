import React, { useEffect, useState } from 'react';
import styles from './header.module.scss';
import logo from '../../assets/images/logo.svg';
import { Button } from '../button/Button.tsx';
import cn from 'classnames';
import { animate, withStartDelay } from '../../utils/animate.ts';

const items = ['Products', 'Pricing', 'FAQ', 'Blog', 'Home'];

export const Header: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  /** Состояние скрола */
  const [scrolled, setScrolled] = useState<boolean>(window.scrollY > 0);

  const toggle = () => setOpen(prev => !prev);

  useEffect(() => {
    const listener = () => {
      const newScrolled = window.scrollY > 0;
      setScrolled(prev => {
        if (prev !== newScrolled) {
          return newScrolled;
        }
        return prev;
      });
    };
    window.addEventListener('scroll', listener);

    return () => window.removeEventListener('scroll', listener);
  }, []);

  return (
    <header className={cn(styles.header, { [styles.scrolled]: scrolled })}>
      <div
        {...animate({ animation: 'fade', delay: withStartDelay() })}
        className={styles.container}
      >
        <a className={styles.logo} href='#'>
          <img src={logo} alt='logo' />
        </a>
        <nav
          className={cn(styles.nav, {
            [styles.navOpen]: open,
          })}
        >
          {items.map(item => (
            <a className={styles.navLink} href='#' key={item}>
              {item}
            </a>
          ))}
        </nav>
        <div className={styles.buttons}>
          <a className={styles.signin} href='#'>
            Sign in
          </a>
          <Button type='ghost' className={styles.signup}>
            Sign up
          </Button>
          <Button
            className={cn(styles.menu, {
              [styles.menuOpen]: open,
            })}
            onClick={toggle}
          >
            <span />
          </Button>
        </div>
      </div>
    </header>
  );
};
