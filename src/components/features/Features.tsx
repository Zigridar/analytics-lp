import React from 'react';
import styles from './features.module.scss';
import { Icons } from './components/icons/Icons.tsx';
import { Cards } from './components/cards/Cards.tsx';
import { animate } from '../../utils/animate.ts';

export const Features: React.FC = () => (
  <section className={styles.section}>
    <div className={styles.angleWrapper}>
      <div className={styles.revertWrap}>
        <div className={styles.container}>
          <div {...animate({ animation: 'zoom-in' })}>
            <h2 className={styles.header}>Main Features</h2>
            <p className={styles.sub}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum
              nisi aliquet volutpat pellentesque volutpat est. Sapien in etiam
              vitae nibh nunc mattis imperdiet sed nullam. Vitae et, tortor
              pulvinar risus pulvinar sit amet. Id vel in nam malesuada.
            </p>
          </div>
          <Icons className={styles.icons} />
          <Cards />
        </div>
      </div>
    </div>
  </section>
);
