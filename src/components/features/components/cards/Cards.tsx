import React from 'react';
import screen1 from '../../../../assets/images/screen-01.png';
import screen2 from '../../../../assets/images/screen-02.png';
import screen3 from '../../../../assets/images/screen-03.png';
import styles from './cards.module.scss';
import cn from 'classnames';
import { animate } from '../../../../utils/animate.ts';

type Card = {
  img: string;
  title: React.ReactNode;
  text: string;
  direction: 'rtl' | 'ltr';
};

const cards: Array<Card> = [
  {
    title: (
      <>
        Automated Reports
        <br /> & Widget Alerts
      </>
    ),
    img: screen1,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum nisi aliquet volutpat pellentesque volutpat est. Sapien in etiam vitae nibh nunc mattis imperdiet sed nullam. Vitae et, tortor pulvinar risus pulvinar sit amet.',
    direction: 'ltr',
  },
  {
    title: 'Fully customizable to address your needs',
    img: screen2,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum nisi aliquet volutpat pellentesque volutpat est. Sapien in etiam vitae nibh nunc mattis imperdiet sed nullam. Vitae et, tortor pulvinar risus pulvinar sit amet.',
    direction: 'rtl',
  },
  {
    title: 'Pre-built Dashboard Templates',
    img: screen3,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elementum nisi aliquet volutpat pellentesque volutpat est. Sapien in etiam vitae nibh nunc mattis imperdiet sed nullam. Vitae et, tortor pulvinar risus pulvinar sit amet.',
    direction: 'ltr',
  },
];

export const Cards: React.FC = () => (
  <div className={styles.container}>
    {cards.map(({ title, direction, img, text }) => (
      <div
        key={img}
        className={cn(styles.card, { [styles.rtl]: direction === 'rtl' })}
        {...animate({
          animation: direction === 'ltr' ? 'fade-right' : 'fade-left',
          duration: 600,
          offset: 250,
        })}
      >
        <div className={styles.textWrap}>
          <h3 className={styles.title}>{title}</h3>
          <p className={styles.text}>{text}</p>
        </div>
        <img className={styles.chart} src={img} alt='chart' />
      </div>
    ))}
  </div>
);
