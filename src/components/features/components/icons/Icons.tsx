import React from 'react';
import styles from './icons.module.scss';
import monitoring from '../../../../assets/images/monitoring.svg';
import widgets from '../../../../assets/images/widgets.svg';
import performance from '../../../../assets/images/performance.svg';
import cn from 'classnames';
import { animate } from '../../../../utils/animate.ts';

type IconCard = {
  icon: string;
  title: string;
  text: string;
};

const iconCards: Array<IconCard> = [
  {
    icon: monitoring,
    title: 'Monitoring 24/7',
    text: 'Lorem ipsum dolor sit amet, consectetur adipis cing elit. Elementum nisi aliquet volutpat.',
  },
  {
    icon: widgets,
    title: 'Widget System',
    text: 'Sapien in etiam vitae nibh nunc mattis imperdiet sed nullam. Vitae et, tortor pulvinar risus pulvinar.',
  },
  {
    icon: performance,
    title: 'Best Performance',
    text: 'Lorem ipsum dolor sit amet, consectetur adipis cing elit. Elementum nisi aliquet volutpat.',
  },
];

export const Icons: React.FC<IClassName> = ({ className }) => (
  <div className={cn(className, styles.cardContainer)}>
    {iconCards.map(({ icon, title, text }, index) => (
      <div
        {...animate({
          animation: 'zoom-in-right',
          delay: (index + 1) * 100,
          offset: 50,
        })}
        key={title}
        className={styles.card}
      >
        <img className={styles.icon} src={icon} alt='icon' />
        <h3 className={styles.title}>{title}</h3>
        <p className={styles.text}>{text}</p>
      </div>
    ))}
  </div>
);
