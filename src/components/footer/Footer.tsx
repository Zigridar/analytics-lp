import React from 'react';
import styles from './footer.module.scss';
import logo from '../../assets/images/logo-gray.svg';
import instagram from '../../assets/images/Instagram.svg';
import twitter from '../../assets/images/Twitter.svg';
import facebook from '../../assets/images/Facebook.svg';
import subtract from '../../assets/images/subtract.svg';
import { animate } from '../../utils/animate.ts';

const links: Array<string> = ['Product', 'Pricing Plans', 'FAQ', 'Blog'];

const icons: Array<string> = [facebook, twitter, instagram];

export const Footer: React.FC = () => (
  <footer className={styles.footer}>
    <div {...animate({ animation: 'fade' })} className={styles.inner}>
      <div className={styles.wrap}>
        <img src={logo} alt='logo' />
        <nav className={styles.nav}>
          {links.map(item => (
            <a className={styles.link} key={item} href='#'>
              {item}
            </a>
          ))}
        </nav>
        <div className={styles.icons}>
          {icons.map(item => (
            <a key={item} href='#'>
              <img src={item} alt='follow' />
            </a>
          ))}
        </div>
      </div>
    </div>
    <div className={styles.placeholder}>
      <img className={styles.subtract} src={subtract} alt='subtract' />
    </div>
  </footer>
);
