import React, { PropsWithChildren, useEffect } from 'react';
import './animation.scss';
import AOS from 'aos';

export const AnimationProvider: React.FC<PropsWithChildren> = ({
  children,
}) => {
  useEffect(() => {
    setTimeout(() => {
      AOS.init({
        disable: 'mobile',
      });
    }, 100);
  }, []);

  return <>{children}</>;
};
