import { anchorPlacementOptions, easingOptions } from 'aos';

type AnimateOptions = {
  delay?: number;
  offset?: number;
  duration?: number;
  anchor?: string;
  easing?: easingOptions;
  placement?: anchorPlacementOptions;
  once?: boolean;
  animation:
    | 'fade'
    | 'fade-up'
    | 'fade-down'
    | 'fade-left'
    | 'fade-right'
    | 'fade-up-right'
    | 'fade-up-left'
    | 'fade-down-right'
    | 'fade-down-left'
    | 'flip-up'
    | 'flip-down'
    | 'flip-left'
    | 'flip-right'
    | 'slide-up'
    | 'slide-down'
    | 'slide-left'
    | 'slide-right'
    | 'zoom-in'
    | 'zoom-in-up'
    | 'zoom-in-down'
    | 'zoom-in-left'
    | 'zoom-in-right'
    | 'zoom-out'
    | 'zoom-out-up'
    | 'zoom-out-down'
    | 'zoom-out-left'
    | 'zoom-out-right';
};

export const animate = ({
  delay = 0,
  offset = 120,
  duration = 400,
  anchor,
  placement,
  animation,
  once = true,
  easing = 'ease',
}: AnimateOptions) => ({
  'data-aos': animation,
  'data-aos-delay': delay,
  'data-aos-offset': offset,
  'data-aos-duration': duration,
  'data-aos-anchor': anchor,
  'data-aos-anchor-placement': placement,
  'data-aos-once': once,
  'data-aos-easing': easing,
});

export const withStartDelay = (delay = 0) => delay + 500;
