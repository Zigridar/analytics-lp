import React from 'react';
import './styles/index.scss';
import { Header } from './components/header/Header.tsx';
import { Main } from './components/main/Main.tsx';
import { Footer } from './components/footer/Footer.tsx';
import { AnimationProvider } from './utils/AnimationProvider.tsx';

export const App: React.FC = () => (
  <AnimationProvider>
    <Header />
    <Main />
    <Footer />
  </AnimationProvider>
);
